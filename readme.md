This repository contains the code for the paper
```
Scientific Information Extraction with Semi-supervised Neural Tagging, Yi Luan, Mari Ostendorf, Hannaneh Hajishirzi, EMNLP, 2017
```
The code is based on Theano and the work of Lample, et.al 2016, to train the tagger:

```
cd code
./run.sh
```

For the graph based approach, we use the script from the author of the following paper
```
Graph-based semi-supervised acoustic modeling in DNN-basedspeech recognition. Yuzong Liu and Katrin Kirchhoff. 2014. In IEEE SLT.
```
The code is not publically available due to copyright problem but can be obtained upon request.

More codes and details to come.