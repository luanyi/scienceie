outfid = open('./output_sample.txt','w')
for line in open('eval.17.output.tst'):
    if line.rstrip():
        ele = line.rstrip().split()
        new_ele = [ele[0], ele[-2], ele[-1]]
        new_line = ' '.join(new_ele)
        outfid.write(new_line + '\n')
    else:
        outfid.write('\n')
outfid.close()
