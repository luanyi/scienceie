from itertools import izip
import pdb
import os

eval_path = "/homes/luanyi/pubanal/project/code/ScienceKG/eval"
eval_script = os.path.join(eval_path, "conlleval")
cats = ['p','m','c']
asp1 = 'tst'
asp2 = 'test'
name = 'semevalwikiCS_init_250'
dev = '/homes/luanyi/pubanal/project/code/tagger_add_feature/evaluation/results/zeros=False,char_dim=25,char_lstm_dim=25,word_dim=250,word_lstm_dim=100,all_emb=False,cap_dim=4,dropout=0.5,lr_method=sgd-lr_.005,pos_dim=10,dep_dim=0,train=train.feats.cat,train_true=,reload=None,201701271513314/eval.50.output.' + asp1
# dev = '/homes/luanyi/pubanal/project/code/tagger_add_feature/evaluation/results/zeros=False,char_dim=25,char_lstm_dim=25,word_dim=250,word_lstm_dim=100,all_emb=False,cap_dim=4,dropout=0.5,lr_method=sgd-lr_.005,pos_dim=10,dep_dim=0,train=train.feats.cat,train_true=,reload=None,201701271513315/eval.35.output.'+asp1
# dev = './evaluation/results/zeros=False,char_dim=25,char_lstm_dim=25,word_dim=250,word_lstm_dim=100,all_emb=False,cap_dim=4,dropout=0.5,lr_method=sgd-lr_.005,pos_dim=10,dep_dim=0,train=unlabeled_corpus.txt,train_true=,reload=1,201701261520189/eval.45.output.'+asp1 #logs/corpus_semi_wiki.log
# dev = '/atm/turkey/vol/projects/pubanal/luanyi/code/tagger_add_feature/evaluation/results/zeros=False,char_dim=25,char_lstm_dim=25,word_dim=250,word_lstm_dim=100,all_emb=False,cap_dim=4,dropout=0.5,lr_method=sgd-lr_.005,pos_dim=10,dep_dim=0,train=train.feats.cat,train_true=,reload=None,201701242129663/eval.17.output.'+asp1 #logs_bb/wiki_init_250_new2_cat.log
# dev = './evaluation/results/zeros=False,char_dim=25,char_lstm_dim=25,word_dim=250,word_lstm_dim=100,all_emb=False,cap_dim=4,dropout=0.5,lr_method=sgd-lr_.005,pos_dim=10,dep_dim=0,train=train.feats.cat,train_true=,reload=None,201701242219367/eval.23.output.'+asp1 #logs/semieval_init_250_1.log
#
# dev = './evaluation/results/zeros=False,char_dim=25,char_lstm_dim=25,word_dim=250,word_lstm_dim=100,all_emb=False,cap_dim=4,dropout=0.5,lr_method=sgd-lr_.005,pos_dim=10,dep_dim=0,train=train.feats.cat,train_true=,reload=None,201701242219854/eval.25.output.'+asp1
# dev = './evaluation/results/zeros=False,char_dim=25,char_lstm_dim=25,word_dim=250,word_lstm_dim=100,all_emb=False,cap_dim=4,dropout=0.5,lr_method=sgd-lr_.005,pos_dim=10,dep_dim=0,train=train.feats.cat,train_true=,reload=None,201701242219850/eval.26.output.' + asp1
for cat in cats:
    fid1 = open(dev)
    info = '/atm/nest/vol/home/luanyi/pubanal/project/code/tagger_add_feature/data/'+asp2+'.feats.cat'
    fid2 = open(info)
    sentence = []
    sentences = []
    for line1, line2 in izip(fid1, fid2):
        line1 = line1.rstrip()
        if not line1:
            if field == cat:
                if len(sentence) > 0:
                    sentences.append(sentence)
            sentence= []
        else:
            field = line2.rstrip().split()[-1]
            sentence.append(line1)
    output_path ='./field_results/output.'+asp1+'.'+cat+'.'+name 
    scores_path = './field_results/score.'+asp1+'.'+cat+'.'+name 
    fid = open(output_path,'w')

    for sentence in sentences:
        for line in sentence:
            fid.write(line + '\n')
        fid.write('\n')
    fid.close()
    
    os.system("%s < %s > %s" % (eval_script, output_path, scores_path))
